#!/bin/bash -ex

#URL=`curl -I -L http://www.amazon.com/kindlemacdownload 2>/dev/null | grep '^ETag' | tail -1 | sed 's/ETag\: //' | tr -d '\r'`

#if [ "x${URL}" != "x" ]; then
#    echo URL: "${URL}"
#    echo "${URL}" > current-url
#fi

## Update to handle distributed builds
#if cmp current-url old-url; then
#    # Files are identical, exit 1 to NOT trigger the build job
#    exit 1
#else
#    # Files are different - copy marker, exit 0 to trigger build job
#    cp current-url old-url
#    exit 0
#fi


#!/bin/bash -ex

#download dmg
#NEWLOC=`curl -L -o "http://www.amazon.com/kindlemacdownload" 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | tail -1 | sed 's@\.\/@@'`

curl -L -o kindle.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "http://www.amazon.com/kindlemacdownload"

#mount downloaded DMG
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse kindle.dmg | awk '/private\/tmp/ { print $3 } '`

#locate app within downloaded DMG
app_in_dmg=$(ls -d $mountpoint/*.app)

# Obtain version info from APP
VERSION=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" $app_in_dmg/Contents/Info.plist`

if [ "x${VERSION}" != "x" ]; then
	echo "${VERSION}"
fi

hdiutil detach "${mountpoint}"

rm -rf kindle.dmg


if [ "x${VERSION}" != "x" ]; then
    echo version: "${VERSION}"
    echo "${VERSION}" > current-version
fi

# Update to handle distributed builds
if cmp current-version old-version; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-version old-version
    exit 0
fi